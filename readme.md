**REST API untuk Mengakses Profil Mahasiswa di Universitas Mercu Buana**

REST API ini berfungsi untuk mengakses profil mahasiswa di Universitas Mercu Buana. Data diperoleh langsung dari situs resminya di student.mercubuana.ac.id. REST API ini memanfaatkan Fat Free Framework dan Guzzle.

## Kebutuhan Minimal Server

REST API ini akan berjalan dengan baik di server yang menjalankan PHP 7.0 ke atas. Apabila terjadi kendala, silakan merujuk kebutuhan minimal server Fat Free Framework dan Guzzle.

## Pemasangan

Berikut langkah-langkah untuk memasang REST API ini :

* Tarik kode sumber

```
git clone https://gitlab.com/naufal/mercubuana-student-restapi.git
```

* Unduh dan jalankan composer

```
curl -sS https://getcomposer.org/installer | php
```

* Unduh dan pasang komponen yang dibutuhkan

```
php composer.phar update
```

## Penggunaan

* Pastikan komputer terhubung dengan Internet.

* Jalankan web server, bisa langsung menggunakan PHP.

```
php -S localhost:8080
```

* Kunjungi route /api/mercubuana/student/nim/{nim} melalui web browser, misal

```
localhost:8080/api/mercubuana/student/nim/41513010099
```

## Izin Pemakaian

Pemakaian REST API ini diizinkan selama memenuhi syarat lisensi dari MIT.

**Copyright (c) 2016 Naufal Fachrian**

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
