<?php
require 'vendor/autoload.php';

$app = require('vendor/bcosca/fatfree-core/base.php');

$app->config('config.ini');

$app->route('GET /api/mercubuana/student/nim/@nim',
	function($app, $params) {
        if (strlen($params['nim']) != 11 or ctype_digit($params['nim']) == false) {
            $app->error('400');
        }
        $client = new GuzzleHttp\Client();
        $html_response = $client->get($params['nim'] . '.student.mercubuana.ac.id');
        $html = $html_response->getBody();
        $document = new DOMDocument();
        @$document->loadHTML($html);
        $document->preserveWhiteSpace = false;
        $table = $document->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        $student = array ();
        foreach ($rows as $row) {
            $cols = $row->getElementsByTagName('td');
            $key = $cols[0]->textContent;
            $val = ucwords(strtolower(trim(str_replace('  ', '', $cols[2]->textContent))));
            $student[$key] = $val;
        }
        if ($student['NIM'] === $params['nim']) {
            echo json_encode($student);
        } else {
            $app->error('404');
        }
	}
);

$app->route('GET /',
	function($app) {
		$app->error('401');
	}
);

$app->run();
